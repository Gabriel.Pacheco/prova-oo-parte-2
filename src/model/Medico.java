package model;

public class Medico extends Usuario {
	private double salario;
	private double horasPorDia;
	private String area;
	private String hospital;
	private String estado;
	
	
	public double getSalario() {
		return salario;
	}
	public void setSalario(double salario) {
		this.salario = salario;
	}
	public double getHorasPorDia() {
		return horasPorDia;
	}
	public void setHorasPorDia(double horasPorDia) {
		this.horasPorDia = horasPorDia;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
}
